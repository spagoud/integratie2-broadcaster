package be.kdg.broadcastmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class BroadcastmicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BroadcastmicroserviceApplication.class, args);
    }

}
