package be.kdg.broadcastmicroservice.controller;

import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
public class BroadcastController {

    private final SimpMessageSendingOperations template;

    public BroadcastController(SimpMessageSendingOperations template) {
        this.template = template;
    }

    @PostMapping("/broadcast/lobby/{lobbyId}/event")
    public void broadcastEvent(@PathVariable Long lobbyId, @RequestBody String eventJson){
        System.out.println("Event send to lobby: " + lobbyId);
        System.out.println("Event : " + eventJson);

        template.convertAndSend("/lobby/" + lobbyId + "/event", eventJson);
    }

    @PostMapping("/broadcast/lobby/{lobbyId}/game")
    public void broadcastGameUpdate(@PathVariable Long lobbyId, @RequestBody String gameUpdateJson){
        System.out.println("game updated for lobby: " + lobbyId);
        System.out.println("Game update: " + gameUpdateJson);
        template.convertAndSend("/lobby/" + lobbyId + "/game", gameUpdateJson);
    }

    @PostMapping("/broadcast/lobby/{lobbyId}/gamestart")
    public void broadcastGameStart(@PathVariable Long lobbyId){
        System.out.println("game updated for lobby: " + lobbyId);
        System.out.println("Game starting: " + true);
        template.convertAndSend("/lobby/" + lobbyId + "/gamestart", "true");
    }

    @PostMapping("broadcast/lobby/{lobbyId}/gameend")
    public void broadcastGameEnd(@PathVariable Long lobbyId, @RequestBody boolean goalReached){
        System.out.println("Game updated for lobby: " + lobbyId);
        System.out.println("Game ending: " + true);
        template.convertAndSend("/lobby" + lobbyId + "/gameend", goalReached);
    }

    @PostMapping("broadcast/lobby/{lobbyId}/gamepauzed")
    public void broadcastGamePauzed(@PathVariable Long lobbyId) {
        System.out.println("Game updated for lobby: " + lobbyId);
        System.out.println("Game pauzing: " + true);
        template.convertAndSend("/lobby" + lobbyId + "/gamepauzed", "true");
    }
    
    @PostMapping("/broadcast/lobby/{lobbyId}/lobbyUpdate")
    public void broadcastLobbyUpdate(@PathVariable Long lobbyId, @RequestBody String lobbyUpdateJson) {
        System.out.println("lobby updated for lobby: " + lobbyId);
        System.out.println("Lobby update: " + lobbyUpdateJson);
        template.convertAndSend("/lobby/" + lobbyId + "/lobbyUpdate", lobbyUpdateJson);
    }
}
