package be.kdg.broadcastmicroservice.controller;

import be.kdg.broadcastmicroservice.model.ChatMessage;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
public class ChatController {

    private SimpMessageSendingOperations template;

    public ChatController(SimpMessageSendingOperations template) {
        this.template = template;
    }
/*
    @MessageMapping("/chat/sendMessage/{lobbyId}")
    public void sendMessage(@Payload ChatMessage chatMessage, @DestinationVariable Long lobbyId){

        template.convertAndSend("/lobby/chat/" + lobbyId, chatMessage);
    }

    @MessageMapping("/chat/addUser/{lobbyId}")
    public void addUser(@Payload ChatMessage chatMessage, @DestinationVariable Long lobbyId, SimpMessageHeaderAccessor headerAccessor){

        //add username in websocket session
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());

        template.convertAndSend("/lobby/chat/" + lobbyId, chatMessage);
    }*/

    @PostMapping("/chat/sendMessage/{lobbyId}")
    public void sendMessage(@PathVariable Long lobbyId, @RequestBody String chatMessageJson){
        System.out.println("Event send to lobby: " + lobbyId);
        System.out.println("Event : " + chatMessageJson);
        template.convertAndSend("/lobby/chat/" + lobbyId, chatMessageJson);
    }
}
