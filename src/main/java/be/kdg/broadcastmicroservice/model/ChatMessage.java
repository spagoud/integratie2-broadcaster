package be.kdg.broadcastmicroservice.model;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class ChatMessage {
    private MessageType type;
    private String content;
    private String sender;

    public enum MessageType {
        INFO,
        CHAT,
        JOIN,
        LEAVE
    }
}

